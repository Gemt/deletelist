-- Author      : G3m7
-- Create Date : 6/1/2019 6:56:48 PM
-- add ignore list?
local numSlots = 20
local slotHeight = 24

local MainFrame = CreateFrame("BUTTON", "CLGuide_DeleteCheapest", UIParent)
MainFrame:SetClampedToScreen(true)
MainFrame:SetPoint("TOPLEFT", "UIParent", "TOPLEFT", 0, -200)
MainFrame:SetScale(1.4)
MainFrame:SetWidth(80)
--MainFrame:SetHeight(200)
MainFrame:SetHeight(20)
MainFrame:SetMovable(true)
MainFrame:EnableMouse(true)
MainFrame:SetUserPlaced(true)

MainFrame:SetBackdrop{
	bgFile = "Interface\\BUTTONS\\WHITE8X8",
	insets = { left = 1, right = 1, top = 1, bottom = 1	 }
}
MainFrame:SetBackdropColor(0, 0, 0, 0.5)
MainFrame:RegisterForDrag("LeftButton")
MainFrame:SetScript("OnDragStart", function(self)
    if IsAltKeyDown() then 
        self:StartMoving() 
    end
end)
MainFrame:SetScript("OnDragStop", function(self)
    self:StopMovingOrSizing()
end)

MainFrame.VendorValueText = MainFrame:CreateFontString(nil,"ARTWORK")
MainFrame.VendorValueText:SetFont("Fonts\\FRIZQT__.TTF", 7)
MainFrame.VendorValueText:SetPoint("TOPLEFT", MainFrame, "TOPLEFT", 5, -5)


CheapestItemsFrameBackground = CreateFrame("Frame", nil, MainFrame)
CheapestItemsFrameBackground:SetPoint("TOPLEFT", MainFrame, "BOTTOMLEFT", 0, 2)
CheapestItemsFrameBackground:SetHeight(math.min(numSlots*(slotHeight+2), 200))
CheapestItemsFrameBackground:SetWidth(80)
CheapestItemsFrameBackground:SetBackdrop{
	bgFile = "Interface\\BUTTONS\\WHITE8X8",
	insets = { left = 1, right = 1, top = 1, bottom = 1	 }
}
CheapestItemsFrameBackground:SetBackdropColor(0, 0, 0, 0.5)

CheapestItemsFrame = CreateFrame("Frame", "CLGuide_CheapestItemsFrame", CheapestItemsFrameBackground)
CheapestItemsFrame:SetPoint("TOPLEFT", CheapestItemsFrameBackground, "TOPLEFT",-5, 0)
CheapestItemsFrame:SetPoint("BOTTOMRIGHT", CheapestItemsFrameBackground, "BOTTOMRIGHT",0, 0)

--CheapestItemsFrame:SetPoint("BOTTOMRIGHT", MainFrame, "BOTTOMRIGHT", 0,0)

CheapestItemsFrame.Scroll = CreateFrame("ScrollFrame", nil, CheapestItemsFrame, "UIPanelScrollFrameTemplate")
CheapestItemsFrame.Scroll:SetPoint("TOPLEFT", CheapestItemsFrame, "TOPLEFT", 0, 0)
CheapestItemsFrame.Scroll:SetPoint("BOTTOMRIGHT", CheapestItemsFrame, "BOTTOMRIGHT", 0, 0)
local child = CreateFrame("Frame", nil, CheapestItemsFrame.Scroll)
child:SetHeight(slotHeight*numSlots)
child:SetWidth(CheapestItemsFrame:GetWidth())

CheapestItemsFrame.Scroll:SetScrollChild(child)
CheapestItemsFrame.Scroll.ScrollBar:SetScale(0.7)
CheapestItemsFrame.Scroll.ScrollBar:SetWidth(-30)
CheapestItemsFrame.Scroll.ScrollBar:Hide()
local scrollframes = {}
for i = 1, numSlots do
    local frame = CreateFrame("Frame", "CLGuide_ChepestItemScrollChild"..i, child)
    scrollframes[i] = frame
    
    frame:SetPoint("TOPLEFT", child, "TOPLEFT", 5, -slotHeight*(i-1))
    frame:SetWidth(child:GetWidth()-5)
    frame:SetHeight(slotHeight)
    frame:SetBackdrop{
	    bgFile = "Interface\\BUTTONS\\WHITE8X8",
	    insets = { left = 1, right = 1, top = 1, bottom = 1	 }
	}
    frame:SetBackdropColor(0, 0, 0, 0.5)

    frame.actionButton = CreateFrame("Button", "CLGuide_ChepestItemScrollChildActionBtn"..i, frame, "ActionButtonTemplate")
    frame.actionButton:SetPoint("LEFT", frame, "LEFT", 5, 0)
    frame.actionButton:SetScale(0.5)
    frame.actionButton:SetAttribute("type1", "item")
    frame.actionButton.Count = frame.actionButton:CreateFontString(nil,"ARTWORK")
    frame.actionButton.Count:SetFont("Fonts\\FRIZQT__.TTF", 14, "THICKOUTLINE")
    frame.actionButton.Count:SetPoint("BOTTOMRIGHT", frame.actionButton, "BOTTOMRIGHT", -2, 2)
    frame.text = frame:CreateFontString(nil,"ARTWORK")
    frame.text:SetFont("Fonts\\FRIZQT__.TTF", 6)
    frame.text:SetPoint("LEFT", frame.actionButton, "RIGHT", 1, 0)
    frame.text:SetPoint("RIGHT", frame, "RIGHT", -20, 0)
    frame.text:SetJustifyH("RIGHT")
    frame:Hide()
    
	frame.actionButton:SetScript("OnClick", function() 
        if IsControlKeyDown() ~= true or IsShiftKeyDown() ~= true then return end

        local texture, itemCount, _, quality, _, _, itemLink = GetContainerItemInfo(frame.bag, frame.slot);
        if frame.link == itemLink and frame.count == itemCount then
            GuideWarning("Deleted "..frame.link.."x"..tostring(frame.count))
            ClearCursor()
            PickupContainerItem(frame.bag, frame.slot)
            DeleteCursorItem()
        else
            GuideWarning("Bags have changed and frame is out of date. Not deleting. Move something in bags to trigger an update and re-try.")
        end
    end)
    frame.actionButton:SetScript("OnEnter", function()
        GameTooltip:SetOwner(frame.actionButton)
        GameTooltip:SetBagItem(frame.bag, frame.slot)
    end)
    frame.actionButton:SetScript("OnLeave", function()
        GameTooltip:Hide()
    end)
end

local CurrentMoneyString = ""
local function CLGuide_GetItemValueList()
    CLGuide_Items = {}
    local vendorValue = 0
    local bagValue = 0
    local greenValue = 0
    local freeSlots = 0
    local lvl = UnitLevel("player")
    for bag=0,4 do
		local bagname = GetBagName(bag)
        if bagname ~= nil and string.find(bagname, "Quiver") == nil and string.find(bagname, "Soul") == nil then
			for slot=1,GetContainerNumSlots(bag)  do
                local texture, itemCount, _, quality, _, _, itemLink = GetContainerItemInfo(bag, slot);
                if texture ~= nil then
                    local name,_,_,_,_,_,_,_,_,_,price=GetItemInfo(itemLink)
                    if price ~= nil and price > 0 then
                        local slotValue = price*itemCount
                        bagValue = bagValue + slotValue
                        local isGreyItem = (string.find(itemLink, "ff9d9d9d") ~= nil)
                        if isGreyItem then
                            vendorValue = vendorValue + slotValue
                        elseif CLGuide_VendorList ~= nil then
                            local vendorListItm = CLGuide_VendorList[name]
                            if vendorListItm ~= nil and lvl >= vendorListItm then
                                vendorValue = vendorValue + slotValue
                            elseif quality == 2 then
                                greenValue = greenValue + slotValue
                            end
                        end
                        table.insert(CLGuide_Items, {bag=bag,slot=slot,link=itemLink,count=itemCount,value=slotValue,texture=texture,name=name})
                    end
                else
                    freeSlots = freeSlots + 1
                end
			end
		end
	end

    local currentMoney = GetMoney()
    local postAutoValue = currentMoney + vendorValue
    local postAllValue = currentMoney + bagValue
    local PostAutoAndgreen = currentMoney + vendorValue + greenValue
    CurrentMoneyString = string.format("   Current %s\n+ Vendor %s\n+ Greens %s",
       GetMoneyString(currentMoney),
       GetMoneyString(postAutoValue),
       GetMoneyString(PostAutoAndgreen))
    
    MainFrame.VendorValueText:SetText(GetMoneyString(postAutoValue).." ("..tostring(freeSlots)..")")
    table.sort(CLGuide_Items, function(a,b) return a.value<b.value end)
    return CLGuide_Items
end

local function UpdateDeleteFrame()
    local items = CLGuide_GetItemValueList()
    local x = 0
    for i, n in ipairs(items) do
        x = i
        if x > numSlots then return end
        local frame = scrollframes[i]
        frame.bag = n.bag
        frame.slot = n.slot
        frame.count = n.count
        frame.link = n.link
        frame.text:SetText(GetMoneyString(n.value))
        frame.actionButton:SetAttribute("item", n.name)
        frame.actionButton.Count:SetText(tostring(n.count))
        local icon = _G["CLGuide_ChepestItemScrollChildActionBtn"..i.."Icon"]
        icon:SetTexture(n.texture)
        frame:Show()
        if GetMouseFocus() == frame.actionButton then
            GameTooltip:SetOwner(frame.actionButton)
            GameTooltip:SetBagItem(frame.bag, frame.slot)
        end
    end
    -- hide remaining frames, if any
    for x=x+1, numSlots do
        local frame = scrollframes[x]
        frame:Hide()
        frame.bag = nil
        frame.slot = nil
        frame.count = nil
        frame.link = nil
    end
end




MainFrame:SetScript("OnClick", function()
    if CheapestItemsFrameBackground:IsShown() == true then
        CheapestItemsFrameBackground:Hide()
        --MainFrame:SetHeight(20)
    else
        CheapestItemsFrameBackground:Show()
        --MainFrame:SetHeight(200)
        UpdateDeleteFrame()
    end
end)

local NextBagCheck = 0
MainFrame:RegisterEvent("BAG_UPDATE")
MainFrame:SetScript("OnEvent", function(self, event, arg1)
    local bagname = GetBagName(arg1)
    if bagname ~= nil and string.find(bagname, "Quiver") == nil and string.find(bagname, "Soul") == nil then
        NextBagCheck = GetTime() + 1
    end
end)

MainFrame:SetScript("OnUpdate", function() 
    if NextBagCheck ~= nil and GetTime() > NextBagCheck then
        NextBagCheck = nil
        UpdateDeleteFrame()
    end
end)

MainFrame:SetScript("OnEnter", function(self)
    GameTooltip:SetOwner(self)
    GameTooltip:AddLine(CurrentMoneyString)
    GameTooltip:Show()
end)

MainFrame:SetScript("OnLeave", function(self)
    if GameTooltip:GetOwner() == self then
        GameTooltip:Hide()
    end
end)
